import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AttainmentRoutingModule } from './attainment-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AttainmentRoutingModule
  ]
})
export class AttainmentModule { }
